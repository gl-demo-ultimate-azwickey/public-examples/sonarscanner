# -*- coding: utf-8 -*-

import json
import os
from pprint import pprint

import html2markdown
import requests
import string_utils
from envparse import Env

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


SONAR_HOST_URL = env('SONAR_HOST_URL', default='http://localhost:9000')

lang_translation = {
    'py': 'python',
    # 'web': 'web',
    'cs': 'csharp',
    'css': 'css',
    'flex': 'flex',
    'go': 'go',
    'java': 'java',
    'js': 'javascript',
    # 'jsp': 'jsp',
    'kotlin': 'kotlin',
    'php': 'php',
    'ruby': 'ruby',
    'scala': 'scala',
    'ts': 'typescript',
    'vbnet': 'vbnet',
    'xml': 'xml'
}


def cache_rules():
    if os.path.isfile('rules.json'):
        with open('rules.json', 'r') as f:
            rules = json.loads(f.read())
        return rules
    page_number = 1
    url = '{SONAR_HOST_URL}/api/rules/search?p={page_number}'.format(
        SONAR_HOST_URL=SONAR_HOST_URL,
        page_number=page_number,
    )
    auth = ('admin', 'admin')
    r = requests.get(url, auth=auth)
    response = r.json()
    rules = {}

    print('Caching Rules!')
    while response.get('rules', []):
        url = '{SONAR_HOST_URL}/api/rules/search?p={page_number}'.format(
            SONAR_HOST_URL=SONAR_HOST_URL,
            page_number=page_number,
        )
        r = requests.get(url, auth=auth)
        response = r.json()
        for rule in response.get('rules', []):
            rules[rule['key']] = rule
        page_number += 1

    with open('rules.json', 'w') as f:
        f.write(json.dumps(rules, indent=4, sort_keys=True))
    print('Caching Complete')
    return rules


def main():
    """
    runs the application
    """
    url = '{SONAR_HOST_URL}/api/issues/search'.format(
        SONAR_HOST_URL=SONAR_HOST_URL,
    )
    auth = ('admin', 'admin')
    rules = cache_rules()
    r = requests.get(url, auth=auth)
    all_issues = []

    response = r.json()

    if not response.get('issues', []):
        print('############# NO ISSUES ##################')
        print(response)
        print('##########################################')

    for issue in response.get('issues', []):

        issue_spec = dict()
        issue_spec['category'] = 'sast'
        rule_name = issue.get('rule')
        if not rule_name:
            print('Skipping issue:', issue)
            continue

        rule = rules.get(rule_name, {})
        if not rule:
            continue
        
        if issue['severity'].lower() not in ['info', 'minor', 'major', 'critical', 'blocker']:
            continue
        
        pprint(issue)
        pprint(rule)

        codequality = {
            "description": issue.get('message'),
            "check_name": rule_name,
            "fingerprint": issue.get('hash'),
            "severity": issue['severity'].lower(),
            "location": {
                "path": issue['component'].rsplit(':', 1)[-1],
                "lines": {
                    "begin": issue.get('textRange', {}).get('startLine'),
                    "end": issue.get('textRange', {}).get('endLine'),
                }
            },
            "content": {
                "body": rule.get('mdDesc')
            },
            "type": rule.get('type')
        }

        all_issues.append(codequality)

    with open('gl-code-quality-report.json', 'w') as f:
        f.write(json.dumps(all_issues, indent=4, sort_keys=True))


def test():
    if os.path.isfile('sample.json'):
        with open('sample.json', 'r') as f:
            sample = json.loads(f.read())

    for issue_spec in sample['vulnerabilities']:
        verify_issue_spec(issue_spec)

    pprint(sample)
    with open('gl-sast-report.json', 'w') as f:
        f.write(json.dumps(sample, indent=4, sort_keys=True))


if __name__ == '__main__':
    if env('SAMPLE', default=None) is not None:
        print('RUNNING SAMPLE')
        test()
    else:
        main()
