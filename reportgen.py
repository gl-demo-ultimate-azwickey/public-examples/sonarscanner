# -*- coding: utf-8 -*-

import json
import os
import pprint

import html2markdown
import requests
import string_utils
from envparse import Env
from datetime import datetime

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


SONAR_HOST_URL = env('SONAR_HOST_URL', default='http://localhost:9000')

lang_translation = {
    'py': 'python',
    # 'web': 'web',
    'cs': 'csharp',
    'css': 'css',
    'flex': 'flex',
    'go': 'go',
    'java': 'java',
    'js': 'javascript',
    # 'jsp': 'jsp',
    'kotlin': 'kotlin',
    'php': 'php',
    'ruby': 'ruby',
    'scala': 'scala',
    'ts': 'typescript',
    'vbnet': 'vbnet',
    'xml': 'xml'
}

gl_results = {
    'version': '15.0.7',
    'vulnerabilities': [],
    'remediations': [],
    'scan': {
        "analyzer": {
            "id": "sonarqube-quality",
            "name": "Sonarsource",
            "url": "https://docs.sonarsource.com/sonarqube/latest/",
            "vendor": {
                "name": "Sonarsource"
            },
            "version": "1.0.2"
        },
        "scanner": {
            "id": "sonarqube-quality",
            "name": "Sonarsource",
            "url": "https://docs.sonarsource.com/sonarqube/latest/",
            "vendor": {
                "name": "Sonarsource"
            },
            "version": "1.0.2"
        },
    }
}


class Validator:

    @staticmethod
    def is_snake_case(x):
        return string_utils.is_snake_case(x) or str(x).islower()

    @staticmethod
    def is_lower_case(x):
        return str(x).islower()


class VV:

    def __init__(self, description, *args, **kwargs):
        """
        Used to create specification validation objects
        :param description:
        :param validators:
        :param required:
        :return:
        """
        self.description = description
        self.validator_names = args
        self.required = kwargs.get('required', False)
        self.possible_values = kwargs.get('possible_values', tuple())
        self.sub_casts = kwargs.get('sub_casts', {})

        if self.possible_values:
            self.possible_values = set(self.possible_values)


specification = dict(
    category=VV(
        'Where this vulnerability belongs (SAST, Dependency Scanning etc.). For SAST, it will always be sast.',
        'is_snake_case',
        required=True,
    ),
    name=VV(
        'Name of the vulnerability, this must not include the occurrence’s specific information.',
    ),
    message=VV(
        'A short text that describes the vulnerability, it may include the occurrence’s specific information.',
    ),
    description=VV(
        'A long text that describes the vulnerability.',
    ),
    cve=VV(
        'A fingerprint string value that represents a concrete occurrence of the vulnerability. Is used to '
        'determine whether two vulnerability occurrences are same or different. May not be 100% accurate. '
        'This is NOT a CVE.',
        required=True
    ),
    severity=VV(
        'How much the vulnerability impacts the software. ',
        possible_values=['Undefined', 'Info', 'Unknown', 'Low', 'Medium', 'High', 'Critical'],
        required=True
    ),
    confidence=VV(
        'How reliable the vulnerability’s assessment is.',
        possible_values=['Undefined', 'Ignore', 'Unknown', 'Experimental', 'Low', 'Medium', 'High', 'Confirmed'],
        required=True
    ),
    solution=VV(
        'Explanation of how to fix the vulnerability.'
    ),
    scanner=VV(
        'A node that describes the analyzer used to find this vulnerability.',
        sub_casts=dict(
            id=VV(
                'Id of the scanner as a snake_case string.',
                'is_snake_case',
                required=True
            ),
            name=VV(
                'Name of the scanner, for display purposes.',
                required=True,
            )
        )
    ),
    location=VV(
        'A node that tells where the vulnerability is located.',
        sub_casts={
            'file': VV('Path to the file where the vulnerability is located.'),
            'start_line': VV('The first line of the code affected by the vulnerability.'),
            'end_line': VV('The last line of the code affected by the vulnerability.'),
            'class': VV('If specified, provides the name of the class where the vulnerability is located.'),
            'method': VV('If specified, provides the name of the method where the vulnerability is located.'),
        },
    ),
    identifiers=VV(
        'An ordered array of references that identify a vulnerability on internal or external DBs.',
        required=True,
        sub_casts={
            'type': VV('Type of the identifier.', required=True, possible_values=['cve', 'cwe', 'osvdb', 'usn', 'bandit_test_id']),
            'name': VV('Name of the identifier for display purposes.', required=True),
            'value': VV('Value of the identifier for matching purposes.', required=True),
            'url': VV('URL to identifier’s documentation.')
        }
    )
)


def verify_issue_spec(issue, spec=None):

    spec = spec or specification

    for name, vv in spec.items():
        try:
            value = issue.get(name)
        except AttributeError:
            continue

        if value is None:
            if vv.required:
                raise Exception(
                    'Must provide a key in order to proceed for %s in issue: %s' %
                    (name, issue.get('name', 'unknown issue'))
                )
            else:
                continue

        for validator_name in vv.validator_names:
            if not getattr(Validator, validator_name)(value):
                raise Exception('Invalid value (%s) due to validator %s' % (value, validator_name))

        if vv.possible_values and not set(value) | vv.possible_values:
            raise Exception('Could not find value %s in list of possible values for %s' % (value, name))

        if vv.sub_casts:
            for sub_issue in [value, ] if isinstance(value, dict) else value:
                if not isinstance(sub_issue, dict):
                    raise Exception('Must be a dictionary, not a: %s' % sub_issue)
                verify_issue_spec(sub_issue, vv.sub_casts)


def cache_rules():
    if os.path.isfile('rules.json'):
        with open('rules.json', 'r') as f:
            rules = json.loads(f.read())
        return rules
    page_number = 1
    url = '{SONAR_HOST_URL}/api/rules/search?p={page_number}'.format(
        SONAR_HOST_URL=SONAR_HOST_URL,
        page_number=page_number,
    )
    auth = ('admin', 'admin')
    r = requests.get(url, auth=auth)
    response = r.json()
    rules = {}

    print('Caching Rules!')
    while response.get('rules', []):
        url = '{SONAR_HOST_URL}/api/rules/search?p={page_number}'.format(
            SONAR_HOST_URL=SONAR_HOST_URL,
            page_number=page_number,
        )
        r = requests.get(url, auth=auth)
        response = r.json()
        for rule in response.get('rules', []):
            rules[rule['key']] = rule
        page_number += 1

    with open('rules.json', 'w') as f:
        f.write(json.dumps(rules, indent=4, sort_keys=True))
    print('Caching Complete')
    return rules


def main():
    """
    runs the application
    """
    start_time = datetime.now()
    url = '{SONAR_HOST_URL}/api/issues/search'.format(
        SONAR_HOST_URL=SONAR_HOST_URL,
    )
    auth = ('admin', 'admin')
    rules = cache_rules()
    r = requests.get(url, auth=auth)
    all_issues = []

    response = r.json()

    if not response.get('issues', []):
        print(response)

    for issue in response.get('issues', []):
        print(issue)
        issue_spec = dict()
        issue_spec['category'] = 'sast'
        rule_name = issue.get('rule')
        if not rule_name:
            print('Skipping issue:', issue)
            continue

        rule = rules.get(rule_name, {})
        if rule.get('type') != 'VULNERABILITY':
            continue

        issue_spec['name'] = rule.get('name')
        issue_spec['message'] = issue.get('message')
        description = rule.get('mdDesc').replace('\n', '<br>')
        issue_spec['description'] = html2markdown.convert(description)

        conversion = {
            'BLOCKER': 'Critical',
            'CRITICAL': 'High',
            'MAJOR': 'Medium',
            'MINOR': 'Low',
            'INFO': 'Info',
        }
        issue_spec['severity'] = conversion.get(issue['severity'], 'Unknown')
        if issue_spec['severity'] not in ['Undefined', 'Info', 'Unknown', 'Low', 'Medium', 'High', 'Critical']:
            issue_spec['severity'] = 'Undefined'
        issue_spec['confidence'] = 'Unknown'

        # BOGUS STUFF BELOW...
        issue_spec["cve"] = "sonarqube"
        issue_spec['scanner'] = {
            "id": rule.get('repo', '').lower(),
            "name": rule.get('repo', ''),
        }
        identifier = rule_name.replace(':', '_').lower()
        sonar_source_url_template = 'https://rules.sonarsource.com/{language}/type/Vulnerability/RSPEC-{rule_id}'
        language, rule_id = rule_name.split(':', 1)
        if len(rule_id) == 0:
            rule_id = ' #'

        issue_spec['identifiers'] = [
            {
                'type': identifier,  # 'phpcs_security_audit_source',
                "name": identifier,  # 'PHPCS_SecurityAudit.BadFunctions.FilesystemFunctions.WarnFilesystem',
                "value": identifier,  # 'PHPCS_SecurityAudit.BadFunctions.FilesystemFunctions.WarnFilesystem',
                "url": sonar_source_url_template.format(language=language, rule_id=rule_id[1:])
            }
        ]

        # END OF BOGUS STUFF...

        issue_spec['location'] = dict(
            file=issue['component'].split(':', 1)[-1],
            dependency={"package": {}}
        )

        # GitLab throws an error if the start_line and end_line are the same.
        if issue.get('textRange', {}).get('startLine', None) is not None:
            issue_spec['location']['start_line'] = issue.get('textRange', {}).get('startLine', None)
            if issue.get('textRange', {}).get('endLine', None) is not None:
                if issue.get('textRange', {}).get('endLine', None) != issue.get('textRange', {}).get('startLine', None):
                    issue_spec['location']['end_line']=issue.get('textRange', {}).get('endLine', None)

        verify_issue_spec(issue_spec)

        issue_spec['id'] = issue['key']

        all_issues.append(issue_spec)
        gl_results['vulnerabilities'].append(issue_spec)

    # gl_results['vulnerabilities'].append(all_issues)
    gl_results['scan']["start_time"]= start_time.strftime('%Y-%m-%dT%H:%M:%S')  #"2023-12-28T14:34:41"
    gl_results['scan']["end_time"] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    gl_results['scan']["status"] = "success"
    gl_results['scan']["type"] = "sast"
    # result = {
    #     "version": "15.0.7",
    #     "vulnerabilities": all_issues,
    #     "remediations": [],
    # }

    with open('gl-sast-report.json', 'w') as f:
        f.write(json.dumps(gl_results, indent=4, sort_keys=True))


def test():
    if os.path.isfile('sample.json'):
        with open('sample.json', 'r') as f:
            sample = json.loads(f.read())

    for issue_spec in sample['vulnerabilities']:
        verify_issue_spec(issue_spec)

    pprint.pprint(sample)
    with open('gl-sast-report.json', 'w') as f:
        f.write(json.dumps(sample, indent=4, sort_keys=True))


if __name__ == '__main__':
    if env('SAMPLE', default=None) is not None:
        print('RUNNING SAMPLE')
        test()
    else:
        main()
