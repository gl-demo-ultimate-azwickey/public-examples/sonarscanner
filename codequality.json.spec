{
    "engine_name": "structure",
    "fingerprint": "{{ hash }}",
    "categories": [
        "Complexity"
    ],
    "check_name": "method_complexity",
    "content": {
        "body": "{{ markdown_body }}"
    },
    "description": "Function `handle` has a Cognitive Complexity of 11 (exceeds 5 allowed). Consider refactoring.",
    "location": {
        "path": "autodevops/management/commands/gitlab.py",
        "lines": {
        "begin": 32,
        "end": 81
        }
    },
    "other_locations": [
        
    ],
    "remediation_points": 750000,
    "severity": "minor",
    "type": "issue"
}