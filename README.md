# SonarScanner

This project aims to generate a JSON file that is compatible with GitLab and 
rely on Code Quality by SonarQube.

Have you ever wanted to use GitLab CI as your Security Scanning orchestrator rather than SonarQube?
This project is aimed at providing Sonar users an easy way to get started with GitLab + Sonar
without having an in-depth knowledge of GitLab or GitLab CI.
What's in the box:

* A Docker Image with SonarScanner setup and ready to work inside of GitLab CI.
* A simple GitLab CI that can be referenced within your GitLab Project.

## Getting Started

```yaml
include:
- remote: https://gitlab.com/gl-demo-ultimate-azwickey/public-examples/sonarscanner/raw/master/Sonar.gitlab-ci.yml
```


`Sample Report Artifact`:

```json
// 20200610101616
// https://gitlab-gold.gitlab.io/-/tpoffenbarger/sast/10/-/jobs/473411159/artifacts/gl-sast-report.json

{
  "remediations": [
    
  ],
  "version": "2.2",
  "vulnerabilities": [
    {
      "category": "sast",
      "confidence": "Unknown",
      "cve": "sonarqube",
      "description": "Databases should always be password protected. The use of a database connection with an empty password is a clear indication of a database that is  \nnot protected.\n\n  \n\nThis rule flags database connections with empty passwords.\n\n  \n\n## Noncompliant Code Example\n\n  \n\n<pre><br/>&lt;?php<br/>  $servername = \"localhost\";<br/>  $username = \"AppLogin\";<br/>  $password = \"\";<br/><br/>  // MySQL<br/>  $conn = new mysqli($servername, $username, $password);<br/>  // MySQL<br/>  $conn = mysqli_connect($servername, $username, $password);<br/>  // PDO way<br/>  $conn = new PDO(\"mysql:host=$servername;dbname=myDB\", $username, $password);<br/>  // Oracle<br/>  $conn = oci_connect($username, $password, \"//localhost/orcl\");<br/>  // MS SQL Server<br/>  $sqlsrvName = \"serverName\\sqlexpress\";<br/>  $sqlsrvConnInfo = array( \"Database\"=&gt;\"myDB\", \"UID\"=&gt;$username, \"PWD\"=&gt;$password);<br/>  $conn = sqlsrv_connect( $sqlsrvName, $sqlsrvConnInfo);<br/>  // PosgreSQL<br/>  $pgConnInfo = \"host=localhost port=5432 dbname=test user=\" . $username . \" password=\" . $password;<br/>  $conn = pg_connect($pgConnInfo);<br/>?&gt;<br/></pre>\n\n  \n\n## See\n\n  \n\n  \n *    [OWASP Top 10 2017 Category A3](https://www.owasp.org/index.php/Top_10-2017_A3-Sensitive_Data_Exposure) - Sensitive Data Exposure  \n     \n  \n *    [MITRE, CWE-521](https://cwe.mitre.org/data/definitions/521.html) - Weak Password Requirements \n  ",
      "identifiers": [
        {
          "name": "php_s2115",
          "type": "php_s2115",
          "url": "https://rules.sonarsource.com/php/type/Vulnerability/RSPEC-2115",
          "value": "php_s2115"
        }
      ],
      "location": {
        "dependency": {
          "package": {
            
          }
        },
        "file": "web/index.php",
        "start_line": 8
      },
      "message": "Add password protection to this database.",
      "name": "Databases should be password-protected",
      "scanner": {
        "id": "php",
        "name": "php"
      },
      "severity": "Critical"
    }
  ]
}
```

#### [Project Preview](https://gitlab.com/gitlab-gold/tpoffenbarger/sast/10/-/security/vulnerabilities/1137107)

![Formatting due to markdown from Sonarqube](results.png "Results")



## Local Development

```
#### START SonarQube
docker run -d --name sonarqube -p 9000:9000 sonarqube
# OR
docker start sonarqube -p 9000:9000
```

Then submit a local project (on a Mac)

```bash
docker run -e PROJECT_NAME=${PWD##*/} -e SONAR_HOST_URL=http://host.docker.internal:9000 -it -v "$(pwd):/usr/src" --entrypoint="" sonarsource/sonar-scanner-cli /bin/bash

# In the Docker Container
sonar-scanner -Dsonar.projectKey=${PROJECT_NAME}
```

OR...

```bash
docker run -e SONAR_HOST_URL=http://host.docker.internal:9000 -it -v "$(pwd):/usr/src" --entrypoint="" sonarsource/sonar-scanner-cli /bin/bash -c "sonar-scanner -Dsonar.projectKey=${PWD##*/}"
```


## Useful Links:

* https://rules.sonarsource.com/
* https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/ci/job_artifact.rb
* https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
* https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html
* https://docs.gitlab.com/ee/user/application_security/sast/#reports-json-format
* [JSON Schema](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/22449b3defef9c3c0623501dbfce967e55d17009/src/security-report-format/definitions/1-0-0)
* [Security Scanner Integration](https://docs.gitlab.com/ee/development/integrations/secure)
* [Severity and Confidence](https://docs.gitlab.com/ee/development/integrations/secure#severity-and-confidence)
* https://community.sonarsource.com/t/properties-sonar-login-and-sonar-password/63320
* https://docs.sonarsource.com/sonarqube/latest/user-guide/user-account/generating-and-using-tokens/
* https://docs.sonarsource.com/sonarqube/latest/instance-administration/security/#:~:text=When%20installing%20SonarQube%2C%20a%20default,Password%3A%20admin
* https://stackoverflow.com/questions/39207108/sonarqube-default-credentials-for-internal-h2-db
* https://docs.sonarsource.com/sonarqube/9.9/setup-and-upgrade/configure-and-operate-a-server/environment-variables/
* https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/sonarscanner-cli/
* 