import os
import sys
import time

import requests

from envparse import Env

env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


SONAR_HOST_URL = env('SONAR_HOST_URL', default='http://localhost:9000')


def main():
    url = '{}/api/system/status'.format(SONAR_HOST_URL)

    attempts = 0
    timed_out = False
    r = None
    try:
        r = requests.get(url)
        while (not r.ok and attempts < 20) or (r.json().get('status', '') == 'STARTING' and attempts < 60):
            time.sleep(2)
            r = requests.get(url)
            sys.stdout.write('.')
            sys.stdout.flush()
            attempts += 1
    except Exception as e:
        print(e)
    print('\n')

    if attempts >= 60 or (r and not r.ok and attempts >= 20):
        timed_out = True

    if r is None or r.json().get('status') != 'UP' or timed_out:
        print('Sonarqube failed to start')
        if r:
            print(r.content)
            print(r.status_code)
        exit(1)
    print('Sonarqube is running!')



if __name__ == '__main__':
    main()
